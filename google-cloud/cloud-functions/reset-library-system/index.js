const Compute = require('@google-cloud/compute');

async function resetLibrarySystem() {
  try {
    const compute = new Compute();
    const vm = compute.zone('us-west1-b').vm('library-system')

    await new Promise((resolve) => vm.reset(resolve))

    console.log('Successfully reset library-system instance')
  } catch (err) {
    console.error(err.message)
  }
}

exports.resetLibrarySystem = resetLibrarySystem
