server {
    listen 80;
    listen [::]:80;
    server_name library-system-api.local www.library-system-api.local;

    resolver 127.0.0.11 valid=30s;
    set $upstream http://node16:3010;

    gzip on;
    gzip_disable "msie6";

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

    access_log /etc/nginx/logs/library-system-api.local.access.log;
    error_log /etc/nginx/logs/library-system-api.local.error.log;

    include /etc/nginx/components/disallow-robots.conf;

    location / {
        if ($http_origin !~* "^https?://(library-system.local|www.library-system.local|library-system-api.local|www.library-system-api.local)$") {
            add_header Access-Control-Allow-Origin "https://library-system.local" always;

            return 403;
        }

        if ($request_method = OPTIONS) {
            add_header Access-Control-Allow-Origin "$http_origin";
            add_header Access-Control-Allow-Methods 'OPTIONS, GET, POST, PUT, DELETE';
            add_header Access-Control-Allow-Headers 'Authorization, Content-Type';
            add_header Access-Control-Max-Age 86400;
            add_header Content-Length 0;

            return 204;
        }

        # #region Preview mode
        # Set $preview_mode to true to enable
        set $preview_mode false;

        # Allow specific routes
        if ($uri ~* "^(/security/authenticate)$") {
            set $preview_mode false;
        }

        # Allow read requests
        if ($request_method ~* "^(GET|HEAD|OPTIONS)$") {
            set $preview_mode false;
        }

        if ($preview_mode = true) {
            add_header Access-Control-Allow-Origin "$http_origin" always;
            add_header Content-Type application/json;

            return 403 '{ "success": false, "reason": "Write operation forbidden in preview mode" }';
        }
        # #endregion Preview mode

        proxy_set_header    Host $http_host;
        proxy_set_header    Upgrade $http_upgrade;
        proxy_set_header    Connection "upgrade";
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Real-IP $remote_addr;
        proxy_set_header    Origin $http_origin;
        proxy_max_temp_file_size    0;
        proxy_http_version  1.1;
        proxy_redirect  off;
        proxy_read_timeout  240s;
        proxy_pass $upstream;
    }
}
