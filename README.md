<!-- omit in toc -->
# Library System Stack

Library System Containerized for cross environment ease of setup and serve as a
proof of concept for [Dock CLI](https://gitlab.com/yvnbunag/dock)

<br/>

---

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Repositories](#repositories)
- [Setup](#setup)
- [Credentials](#credentials)
- [Infrastructure](#infrastructure)
  - [NGINX](#nginx)
  - [MySQL](#mysql)

<br/>

---

## Requirements

1. [Docker Engine](https://www.docker.com/) version 19.03 or higher
2. [Docker Compose](https://docs.docker.com/compose/) version 1.27 or higher
3. [Dock](https://gitlab.com/yvnbunag/dock) version 1.0 or higher

<br/>

---

## Repositories

1. [Library System](https://gitlab.com/yvnbunag/library-system)
2. [Library System API](https://gitlab.com/yvnbunag/library-system-api)

<br/>

---

## Setup

1. Pull the sub modules and switch branches to master
    ```sh
    git submodule update --init --recursive
    git submodule foreach --recursive git checkout master
    ```
2. Add the following to [/etc/hosts](/etc/hosts) system config to route
  library-system requests to the containerized NGINX server
    ```sh
    # For linux, route to network-bridged NGINX server
    172.36.0.2	library-system.local
    172.36.0.2	www.library-system.local
    172.36.0.2	library-system-api.local
    172.36.0.2	www.library-system-api.local

    # For linux and OS X, route to port 80 forwarded NGINX server
    127.0.0.1	library-system.local
    127.0.0.1	www.library-system.local
    127.0.0.1	library-system-api.local
    127.0.0.1	www.library-system-api.local
    ```
3. Register and use repository with dock CLI
    ```sh
    dock register
    dock use library-system
    ```
4. Build containers and run install scripts
    ```sh
    dock build --install
    ```
5. Start the application and API in separate terminals
    ```sh
    cd applications/library-system && dock yarn dev
    ```

    AND

    ```sh
    cd microservices/library-system-api && dock yarn dev
    ```

> You may now access the application in
> [http://library-system.local](http://library-system.local)
> and the API in
> [http://library-system-api.local](http://library-system-api.local)


> See [Library System](https://gitlab.com/yvnbunag/library-system) and
> [Library System API](https://gitlab.com/yvnbunag/library-system-api)
> repositories for additional information
<br/>

---

## Credentials

To log in to the application, see credentials configuration in the
[Library System API](https://gitlab.com/yvnbunag/library-system-api#credentials)
documentation

<br/>

---

## Infrastructure

### NGINX

- Config files are located in
  [infrastructure/nginx/etc/nginx/conf.d](infrastructure/nginx/etc/nginx/conf.d)

- Log files are located in
  [infrastructure/nginx/etc/nginx/logs](infrastructure/nginx/etc/nginx/logs)

- To validate config files
  ```sh
  dock run nginx nginx -t
  ```

- To reload NGINX
  ```sh
  dock run nginx nginx -s reload
  ```

- If NGINX server port forwarding is not needed, comment out the following in
  the [docker-compose.yml](docker-compose.yml) config, under nginx service
  ```sh
      ports:
        - 80:80
  ```

<br/>

### MySQL

- Data are stored in [infrastructure/mysql/var/lib/mysql](infrastructure/mysql/var/lib/mysql)

- Engine configurations are located in
  [infrastructure/mysql/etc/mysql/conf.d](infrastructure/mysql/etc/mysql/conf.d)

- Port `3306` of the container is mapped to the local machine through port
  `33060`, to allow connection from outside the container
  ```sh
  mysql -h 127.0.0.1 -P 33060 -u root
  ```
